const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const app = express();
const cors = require('cors');
//const configClient = require("node-config-client");
require('dotenv/config')

//import routes
const postsRoutes = require('./src/Routes/Create');

//Middleware
app.use(cors());
app.use(bodyparser.json());


//routes
app.use('/product/Create', postsRoutes);


//DbConnection
mongoose.connect(process.env.DB_connection, { useNewUrlParser: true, useUnifiedTopology: true });


//Running port
app.listen(3001);

// ------------------ Eureka Config --------------------------------------------
const Eureka = require('eureka-js-client').Eureka;

const eureka = new Eureka({
  instance: {
    app: 'service-nodejs2',
    hostName: 'localhost',
    ipAddr: '127.0.0.1',
    statusPageUrl: 'http://localhost:3001',
    instanceId: 'service-nodejs2',
    status: 'true',
    port: {
      '$': 3001,
      '@enabled': 'true',
    },
    vipAddress: 'localhost',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    }
  },
  eureka: {
    host: 'localhost',
    port: 8585,
    servicePath: '/eureka/apps/',
    registerWithEureka: 'true',
    fetchRegistry: 'true'
  }
});
eureka.logger.level('debug');
eureka.start(function(error){
  console.log(error || 'complete');
});

//--------------Cloud-----------

  /*configClient.load({
    name:'service-nodejs2', // spring application name
    profiles:['prod','test','dev'], // spring profiles    
    label:'master', // git branch
    location:'localhost:9090', // spring cloud server address
  
  }); */
  