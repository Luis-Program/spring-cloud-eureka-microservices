package com.webdevelopment.springservicerest2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringServiceRest2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringServiceRest2Application.class, args);
    }

}
