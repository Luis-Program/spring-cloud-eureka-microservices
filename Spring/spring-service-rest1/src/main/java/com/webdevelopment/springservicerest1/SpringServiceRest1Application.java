package com.webdevelopment.springservicerest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient

public class SpringServiceRest1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringServiceRest1Application.class, args);
	}

}
