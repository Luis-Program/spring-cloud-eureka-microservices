package com.webdevelopment.springservicerest1.Repository;

import org.springframework.data.repository.CrudRepository;
import com.webdevelopment.springservicerest1.Model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {
    
}
